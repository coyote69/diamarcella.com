<?php 

if(isset($_POST['name'])){
  $name = $_POST['name'];
  $mail = '';
  $phone = '';
  $message = '';
  if(isset($_POST['mail'])) { $mail = $_POST['mail'];}
  if(isset($_POST['phone'])) { $phone = $_POST['phone'];}
  if(isset($_POST['message'])) { $message = $_POST['message'];}

  $titulo = "Mensaje recibido en Diamarcella";
  $msg = '
  <!DOCTYPE html>
  <html lang="es" itemscope itemtype="https://schema.org/">
  <head>
    <meta charset="UTF-8">
    <title>Diamarcella</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet" type="text/css" />
    <style>   
      .nota_doonde {font-size: 10px; padding-left: 10px; padding-right: 10px; margin-bottom: 5px; }
      .instrucciones_doonde {font-size: 12px; padding: 10px; } 
      .table_doonde{background-color: #fff; margin: 0; padding: 0; border-spacing: 0; border: 1px solid #ccc; border-collapse: 0px; width: 640px; border-radius: 4px 4px 0 0; box-shadow: 0 7px 15px rgba(0,0,0,0.5); }
      .img_doonde{border-color: #cfd9d7; border-radius: 4px; margin: 12px; width: 180px; }
      td{vertical-align: top; }
      body{font-family:  "Lato", sans-serif; }
      .promo_precio {font-weight: 600; color: #1dcc6b; }
      .promo_precio_antes{color: red; text-decoration: line-through; font-size: 20px; font-weight: 400; }
      .promo_texto{font-size: 12px; font-weight: 200; }
      .promo_local{width: 90%; background-color: rgba(250, 249, 249, 0.69); margin: 0 auto; padding: 5px; margin-top: 20px; border: 1px solid #ccc; border-radius: 2px; min-height: 110px; }
      .img_negocio{width: 70px; height: 70px; border-radius: 50%; float: left; margin-left: 10px; margin-top: 20px; margin-right: 30px; }
      h1,h2,h3,h4,h5,h6{margin: 0; }
      hr {margin-top: 10px; margin-bottom: 10px; border: 0; border-top: 1px solid #eee; }
    </style>
  </head>
  <body>
    <h3>Tienes una consulta nueva desde Diamarcella</h3>
    Nombre: '.$name.'<br>
    Email: '.$mail.'<br>
    Teléfono: '.$phone.'<br>
    Mensaje: <br><hr>'.$message.'<br><hr><br>

  </body>
  </html>
  ';


  $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
  $cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
  $cabeceras .= 'To: Diamarcella <diamarcella@live.com.ar>'. "\r\n";
  $cabeceras .= 'From: Diamarcella <noreply@tabacan.com>' . "\r\n";
  $cabeceras .= 'Bcc: neotecsoft@gmail.com' . "\r\n";
  $cabeceras .= "X-Priority: 1\r\n";
  $cabeceras .= "X-MSMail-Priority: High\r\n"; 
  $cabeceras .= "X-Mailer: PHP/" . phpversion()."\r\n";


  mail("diamarcella@live.com.ar", $titulo, $msg, $cabeceras,"-fnoreply@tabacan.com");

}

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">

	<title>Diamarcella</title>
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <link type="text/css" rel="stylesheet" href="bower_components/Materialize/dist/css/materialize.min.css"  media="screen,projection"/>

  <meta property="og:site_name" content="Diamarcella">
  <meta property="og:type" content="website">
  <meta property="og:url" content="http://diamarcella.com">
  <meta property="og:description" content="Diamarcella | Pet Clothes Maker - Hacemos que tu mascota sea más feliz, diseñamos ropa y cojines para todos los tamaños, perros, gatos, erizos, hamsters... Por mayor y menor.">
  <meta property="og:title" content="Diamarcella|Pet Clothes Maker">
  <meta property="og:image" content="http://diamarcella.com/img/fs.jpg">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta content="yes" name="apple-mobile-web-app-capable">
  <link rel="canonical" href="http://diamarcella.com" />

  <link rel="stylesheet" type="text/css" href="css/main.css">

<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
</head>
<body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-74402777-1', 'auto');
  ga('send', 'pageview');

</script>
  <header>
   <nav>
    <div class="nav-wrapper teal">
      <a href="#" class="brand-logo"><img src="img/logo.png"  class="logo left white ">
        <strong>Diamarcella <small class="hide-on-small-only">/Clothes Maker</small></strong> </a>
        
      </div>
    </nav>

  </header>
  <main>
   <div class="main-body-home">

     <div id="index-banner" class="parallax-container" >
      <div class="section no-pad-bot">
        <div class="container">
          <div class="row">
            <div class="col m8 white-text text-darken-2">
              <h4 style="background-color: rgba(51, 51, 51, 0.44);" class="">Hacemos ropa para tú mascota</h4>   
              <h3 style="background-color: rgba(51, 51, 51, 0.44);">Pedidos al tel: +56976641257</h3>
              <h4 style="background-color: rgba(51, 51, 51, 0.44);" class="">Ventas por mayor y menor - Chile</h4> 

            </div>
            <div class="col m4 card">
              <div class="card-title">
                <?php if(isset($_POST['name'])){
                  echo '<h4>Mensaje enviado</h4>';
                }?>
                Enviame un mensaje
              </div>
              <div class="card-content">
                <form action="index.php" method="POST">

                  <div class="">
                    <label class="item item-input">
                      <input type="text"  id="name" name="name" placeholder="Tu nombre">
                    </label>
                    <label class="item item-input">
                      <input   id="mail" name="mail" type="email" placeholder="Tu email">
                    </label>
                    <label class="item item-input">
                      <input  name="phone" id="phone" placeholder="Tu teléfono (opcional)">
                    </label>
                    <label class="item item-input">
                      <textarea name="message" id="message"  class="materialize-textarea" placeholder="Mensaje"></textarea>
                    </label>

                  </div>
                  <button type="submit" class="btn" >Enviar mensaje</button>
                </form>
              </div>
            </div>
          </div>
        </div>

      </div>
      <div class="parallax"><img src="img/dogp.jpg" alt="Unsplashed background img 1"></div>
    </div>
    <div class="parallax1">
      <div class="row ">
        <div class="col m8 hide-on-small-only" >
          <div class="card">
            <div class="card-content" id="slides">
            <img class="photos" src="img/flyn.jpg">
                        <img class="photos" src="img/chaleco.jpg">

              <img class="photos" src="https://fbcdn-sphotos-g-a.akamaihd.net/hphotos-ak-xpt1/v/t1.0-9/12687957_1251037254913427_8798119333259448282_n.jpg?oh=919fdc753ff5ec5fa13afbf859979eb2&oe=5755D763&__gda__=1466426083_2a0641f1efb02454b4747f78449b9522">
              <img class="photos" src="https://scontent-mia1-1.xx.fbcdn.net/hphotos-xtp1/v/t1.0-9/12654359_1251036844913468_1624447093029453665_n.jpg?oh=996132520e18ae783aab3412653ac6f6&oe=576B462B">
              <img class="photos" src="https://fbcdn-sphotos-g-a.akamaihd.net/hphotos-ak-xpa1/v/t1.0-9/12246668_1259358810747938_5390359611612637546_n.jpg?oh=985b91feeea3d85d2955953cc1702f8e&oe=57599686&__gda__=1465728748_420cfe91b38e7150732364a6121aef81">
              <img  class="photos" src="https://fbcdn-sphotos-h-a.akamaihd.net/hphotos-ak-xlf1/v/t1.0-9/12744413_1259358934081259_3250587179739998902_n.jpg?oh=b0f1d8deb9ce6ab1a0a6d297e4ca958c&oe=576DBE2E&__gda__=1465061045_dc157acd2c377339c8a2564db4c2843e">
              <img class="photos" src="https://scontent-atl3-1.xx.fbcdn.net/hphotos-xfl1/v/t1.0-9/12734006_1252851044732048_997757988765421995_n.jpg?oh=c4b3c529015095b43a8e56d64cbdc0f7&oe=5757A39A">
              <img class="photos" src="https://fbcdn-sphotos-e-a.akamaihd.net/hphotos-ak-xtf1/v/t1.0-9/12734284_1031522513555640_5093625567664561466_n.jpg?oh=5a10c90edc5d837883ad4191a977d688&oe=575DAB9B&__gda__=1466838579_bcadb44a570287ca3b844521e84b8a8b">
            </div>
          </div>
        </div>
        <div class="col m4">

          <div class="card">
            <div class="card-image">
              <img src="img/dog_drib.gif" >
              <span class="card-title">Ropa para mascotas</span>
            </div>
            <div class="card-content">
              <p>Tenemos una gran variedad de ropa para tú mascota.<br>
                Trabajamos con todas las medidas, desde el más pequeño al más grande 
                para que nuestras mascotas disfruten de la ropa mas cómoda.</p>
              </div>
              <div class="card-action">
                <img style="width: 30px;" src="img/erizo.png">
                <img style="width: 80px;"src="img/dog_cat.png">


              </div>

            </div>
          </div>
        </div>
        <div id="index-banner" class="parallax-container" >
          <div class="section no-pad-bot">
           <div class="row">
                <div class="col s6 m4 l3 instagram-emb">
        <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="6" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAAGFBMVEUiIiI9PT0eHh4gIB4hIBkcHBwcHBwcHBydr+JQAAAACHRSTlMABA4YHyQsM5jtaMwAAADfSURBVDjL7ZVBEgMhCAQBAf//42xcNbpAqakcM0ftUmFAAIBE81IqBJdS3lS6zs3bIpB9WED3YYXFPmHRfT8sgyrCP1x8uEUxLMzNWElFOYCV6mHWWwMzdPEKHlhLw7NWJqkHc4uIZphavDzA2JPzUDsBZziNae2S6owH8xPmX8G7zzgKEOPUoYHvGz1TBCxMkd3kwNVbU0gKHkx+iZILf77IofhrY1nYFnB/lQPb79drWOyJVa/DAvg9B/rLB4cC+Nqgdz/TvBbBnr6GBReqn/nRmDgaQEej7WhonozjF+Y2I/fZou/qAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BCQRIrcEAOZ/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">#cape #clothes #dog #pet #security  chaleco reflactante para paseo nocturno $2.000clp</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">Una foto publicada por Marcela Morales (@diamarcella) el <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2016-02-26T15:16:37+00:00">26 de Feb de 2016 a la(s) 7:16 PST</time></p></div></blockquote>
        </div>
             <div class="col s6 m4 l3 instagram-emb">
        <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="6" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAAGFBMVEUiIiI9PT0eHh4gIB4hIBkcHBwcHBwcHBydr+JQAAAACHRSTlMABA4YHyQsM5jtaMwAAADfSURBVDjL7ZVBEgMhCAQBAf//42xcNbpAqakcM0ftUmFAAIBE81IqBJdS3lS6zs3bIpB9WED3YYXFPmHRfT8sgyrCP1x8uEUxLMzNWElFOYCV6mHWWwMzdPEKHlhLw7NWJqkHc4uIZphavDzA2JPzUDsBZziNae2S6owH8xPmX8G7zzgKEOPUoYHvGz1TBCxMkd3kwNVbU0gKHkx+iZILf77IofhrY1nYFnB/lQPb79drWOyJVa/DAvg9B/rLB4cC+Nqgdz/TvBbBnr6GBReqn/nRmDgaQEej7WhonozjF+Y2I/fZou/qAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BCRha0LkAE4/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">#pet #cape #clothes #security  chaleco reflectante para paseo nocturno</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">Una foto publicada por Marcela Morales (@diamarcella) el <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2016-02-27T02:58:08+00:00">26 de Feb de 2016 a la(s) 6:58 PST</time></p></div></blockquote>
        </div>
            <div class="col s6 m4 l3 instagram-emb">
              <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="6" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAAGFBMVEUiIiI9PT0eHh4gIB4hIBkcHBwcHBwcHBydr+JQAAAACHRSTlMABA4YHyQsM5jtaMwAAADfSURBVDjL7ZVBEgMhCAQBAf//42xcNbpAqakcM0ftUmFAAIBE81IqBJdS3lS6zs3bIpB9WED3YYXFPmHRfT8sgyrCP1x8uEUxLMzNWElFOYCV6mHWWwMzdPEKHlhLw7NWJqkHc4uIZphavDzA2JPzUDsBZziNae2S6owH8xPmX8G7zzgKEOPUoYHvGz1TBCxMkd3kwNVbU0gKHkx+iZILf77IofhrY1nYFnB/lQPb79drWOyJVa/DAvg9B/rLB4cC+Nqgdz/TvBbBnr6GBReqn/nRmDgaQEej7WhonozjF+Y2I/fZou/qAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BCIom76EAFQ/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">#pet #clothes #cushion #diamarcella cojín para mascotas 40cm circular expandible</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">Una foto publicada por Marcela Morales (@diamarcella) el <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2016-02-23T16:07:48+00:00">23 de Feb de 2016 a la(s) 8:07 PST</time></p></div></blockquote>
            </div> 
            <div class="col s6 m4 l3 instagram-emb">
             <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="6" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAAGFBMVEUiIiI9PT0eHh4gIB4hIBkcHBwcHBwcHBydr+JQAAAACHRSTlMABA4YHyQsM5jtaMwAAADfSURBVDjL7ZVBEgMhCAQBAf//42xcNbpAqakcM0ftUmFAAIBE81IqBJdS3lS6zs3bIpB9WED3YYXFPmHRfT8sgyrCP1x8uEUxLMzNWElFOYCV6mHWWwMzdPEKHlhLw7NWJqkHc4uIZphavDzA2JPzUDsBZziNae2S6owH8xPmX8G7zzgKEOPUoYHvGz1TBCxMkd3kwNVbU0gKHkx+iZILf77IofhrY1nYFnB/lQPb79drWOyJVa/DAvg9B/rLB4cC+Nqgdz/TvBbBnr6GBReqn/nRmDgaQEej7WhonozjF+Y2I/fZou/qAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BCIoMXqkAEZ/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">#pet #clothes #diamarcella . cama para mascota 45x45</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">Una foto publicada por Marcela Morales (@diamarcella) el <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2016-02-23T16:04:10+00:00">23 de Feb de 2016 a la(s) 8:04 PST</time></p></div></blockquote>

           </div> 
           <div class="col s6 m4 l3 instagram-emb">
            <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="6" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAAGFBMVEUiIiI9PT0eHh4gIB4hIBkcHBwcHBwcHBydr+JQAAAACHRSTlMABA4YHyQsM5jtaMwAAADfSURBVDjL7ZVBEgMhCAQBAf//42xcNbpAqakcM0ftUmFAAIBE81IqBJdS3lS6zs3bIpB9WED3YYXFPmHRfT8sgyrCP1x8uEUxLMzNWElFOYCV6mHWWwMzdPEKHlhLw7NWJqkHc4uIZphavDzA2JPzUDsBZziNae2S6owH8xPmX8G7zzgKEOPUoYHvGz1TBCxMkd3kwNVbU0gKHkx+iZILf77IofhrY1nYFnB/lQPb79drWOyJVa/DAvg9B/rLB4cC+Nqgdz/TvBbBnr6GBReqn/nRmDgaQEej7WhonozjF+Y2I/fZou/qAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BCJCcPXEAIO/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">#pet #clothes  #hamster  #hedgehog #home casa iglú invierno para mascotas</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">Una foto publicada por Marcela Morales (@diamarcella) el <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2016-02-23T19:53:32+00:00">23 de Feb de 2016 a la(s) 11:53 PST</time></p></div></blockquote>
          </div>
          <div class="col s6 m4 l3 instagram-emb">
            <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="6" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAAGFBMVEUiIiI9PT0eHh4gIB4hIBkcHBwcHBwcHBydr+JQAAAACHRSTlMABA4YHyQsM5jtaMwAAADfSURBVDjL7ZVBEgMhCAQBAf//42xcNbpAqakcM0ftUmFAAIBE81IqBJdS3lS6zs3bIpB9WED3YYXFPmHRfT8sgyrCP1x8uEUxLMzNWElFOYCV6mHWWwMzdPEKHlhLw7NWJqkHc4uIZphavDzA2JPzUDsBZziNae2S6owH8xPmX8G7zzgKEOPUoYHvGz1TBCxMkd3kwNVbU0gKHkx+iZILf77IofhrY1nYFnB/lQPb79drWOyJVa/DAvg9B/rLB4cC+Nqgdz/TvBbBnr6GBReqn/nRmDgaQEej7WhonozjF+Y2I/fZou/qAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BCJCHa-kAHn/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">#pet #hamster  #hedgehog  #clothes casa para mascota iglú invierno</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">Una foto publicada por Marcela Morales (@diamarcella) el <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2016-02-23T19:50:41+00:00">23 de Feb de 2016 a la(s) 11:50 PST</time></p></div></blockquote>
          </div>
          <div class="col s6 m4 l3 instagram-emb">
            <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="6" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAAGFBMVEUiIiI9PT0eHh4gIB4hIBkcHBwcHBwcHBydr+JQAAAACHRSTlMABA4YHyQsM5jtaMwAAADfSURBVDjL7ZVBEgMhCAQBAf//42xcNbpAqakcM0ftUmFAAIBE81IqBJdS3lS6zs3bIpB9WED3YYXFPmHRfT8sgyrCP1x8uEUxLMzNWElFOYCV6mHWWwMzdPEKHlhLw7NWJqkHc4uIZphavDzA2JPzUDsBZziNae2S6owH8xPmX8G7zzgKEOPUoYHvGz1TBCxMkd3kwNVbU0gKHkx+iZILf77IofhrY1nYFnB/lQPb79drWOyJVa/DAvg9B/rLB4cC+Nqgdz/TvBbBnr6GBReqn/nRmDgaQEej7WhonozjF+Y2I/fZou/qAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BCJB62BEAHD/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">#pet #clothes #hamster #home casa para mascota invierno iglú #hedgehog</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">Una foto publicada por Marcela Morales (@diamarcella) el <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2016-02-23T19:48:58+00:00">23 de Feb de 2016 a la(s) 11:48 PST</time></p></div></blockquote>
<script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
          </div>
           
        </div>
      </div>
      <div class="parallax"><img src="img/fabrics-14407_960_720.jpg" alt="Unsplashed background img 1"></div>
    </div>

  </div>
</main>

<footer class="parallax2 page-footer">
  <div class="container parallax2">
    <div class="row">

     <div class="col l4 ">
      <h5 class="white-text">Contáctanos</h5>
      <ul>

        <li><a class="grey-text text-lighten-3" href="tel:+56 2 2405 4848">Teléfono: +56 9 76641257
        </a></li>
        <li>
          <a class="grey-text text-lighten-3" href="mailto:diamarcella@live.com.ar">diamarcella@live.com.ar</a></li>
        </ul>
      </div>
      <div class="col l4 s6">
       <img src="img/pdog.gif" style="width: 100%;">
     </div>
     <div class="col l4  s12">
       <h5 class="white-text">Síguenos</h5>
       <ul>
        <li>
          <a class="grey-text text-lighten-3" target="_blank" href="https://www.instagram.com/diamarcella/">
            <img style="width: 60px;" src="img/instaf.png">
          </a>
          <a class="grey-text text-lighten-3" target="_blank" href="https://www.facebook.com/diamarcellaCL">
            <img style="width: 60px;" src="img/facef.png">
          </a>
        </li>

      </ul>
    </div>    

  </div>

</div>
<div class="footer-copyright blue-grey darken-4">
  <div class="container">
    © Diamarcella 2016 | Made with  <i style="font-size: 12px;" class="material-icons">favorite_border</i>
    Tabacan
  </div>
</div>
</footer>

<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="bower_components/Materialize/dist/js/materialize.min.js"></script>
<script type="text/javascript" src="js/jquery.slides.min.js"></script>
<script async defer src="//platform.instagram.com/en_US/embeds.js"></script>    

<script>
 $(document).ready(function(){
  $('.parallax').parallax();
  $("#slides").slidesjs({
    width: 940,
    height: 528,
    play: {
      active: true,
      auto: true,
      interval: 2000,
      swap: true,
      pauseOnHover: false,
      restartDelay: 2500
    },
    navigation: {
      effect: "fade"
    },
    pagination: {
      effect: "fade"
    },
    effect: {
      fade: {
        speed: 800
      }
    }
  });
});
</script>
</body>
</html>